colorscheme catppuccin-mocha
set number
set termguicolors
set ignorecase smartcase
" use system clipboard
set clipboard+=unnamedplus
" force tabline to always show
set showtabline=2

let mapleader=' '
" paste in command mode
cmap <C-p> <C-r>+
" move windows
nmap <silent> <leader>k :wincmd k<CR>
nmap <silent> <leader>j :wincmd j<CR>
nmap <silent> <leader>h :wincmd h<CR>
nmap <silent> <leader>l :wincmd l<CR>
nmap <silent> <leader><Up> :wincmd k<CR>
nmap <silent> <leader><Down> :wincmd j<CR>
nmap <silent> <leader><Left> :wincmd h<CR>
nmap <silent> <leader><Right> :wincmd l<CR>
" switch tabs
nmap <silent> <leader>n gt
nmap <silent> <leader>p gT
nmap <silent> <leader>1 1gt
nmap <silent> <leader>1 1gt
nmap <silent> <leader>2 2gt
nmap <silent> <leader>3 3gt
nmap <silent> <leader>4 4gt
nmap <silent> <leader>5 5gt
nmap <silent> <leader>6 6gt
nmap <silent> <leader>7 7gt
nmap <silent> <leader>8 8gt
nmap <silent> <leader>9 9gt
" drag tabs
nmap <silent> <leader>N :execute 'silent! tabmove +1'<CR>
nmap <silent> <leader>P :execute 'silent! tabmove -1'<CR>

" https://neovim.io/doc/user/tabpage.html#setting-tabline
set tabline=%!MyTabLine()

function MyTabLine()
  let s = '%#TabLineSel# nvim %#TabLineSuffixSel#'
  for i in range(tabpagenr('$'))
    " select the highlighting
    if i + 1 == tabpagenr()
      let s ..= '%#TabLinePrefixSel#%#TabLineSel#'
    else
      let s ..= '%#TabLinePrefix#%#TabLine#'
    endif
    " set the tab page number (for mouse clicks)
    let s ..= '%' .. (i + 1) .. 'T'
    " the label is made by MyTabLabel()
    let s ..= ' %{MyTabLabel(' .. (i + 1) .. ')} '
    if i + 1 == tabpagenr()
      let s ..= '%#TabLineSuffixSel#'
    else
      let s ..= '%#TabLineSuffix#'
    endif
  endfor
  " after the last tab fill with TabLineFill and reset tab page nr
  let s ..= '%#TabLineFill#%T'
  return s
endfunction

function MyTabLabel(n)
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  let label = bufname(buflist[winnr - 1]) 
  let name = fnamemodify(label, ":t")
  let s = a:n .. " "
  if name == ""
     let s ..= "[No Name]"
  else
    let s ..= name
  endif
  if getbufinfo(buflist[winnr - 1])[0].changed
    let s ..= " " .. "[+]"
  endif
  return s
endfunction

highlight TabLine guifg=#cdd6fa guibg=#45475a
highlight TabLineFill guifg=#a6e3a1 guibg=#1e1e2e
highlight TabLinePrefix guifg=#1e1e2e guibg=#45475a
highlight TabLinePrefixSel guifg=#1e1e2e guibg=#a6e3a1
highlight TabLineSel guifg=#45475a guibg=#a6e3a1
highlight TabLineSuffix guifg=#45475a guibg=#1e1e2e
highlight TabLineSuffixSel guifg=#a6e3a1 guibg=#1e1e2e

lua require('plugins')
lua require('nvim-tree-init')
lua require('lualine-init')
lua require('fzf-init')
lua require('nvim-lspconfig-init')
