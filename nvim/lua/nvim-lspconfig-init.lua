require("lspconfig").rust_analyzer.setup({
  -- https://github.com/neovim/nvim-lspconfig/wiki/Running-language-servers-in-containers
  before_init = function(params)
    params.processId = vim.NIL
  end,
  cmd = { "just", "lsp" },
  -- https://github.com/lspcontainers/lspcontainers.nvim#additional-languages
  root_dir = require'lspconfig/util'.root_pattern(".git", vim.fn.getcwd()),
  settings = {
    ["rust-analyzer"] = {
      check = {
        command = "clippy"
      }
    }
  },
})

vim.api.nvim_set_keymap("n", "<leader>e",
    "<cmd>lua vim.diagnostic.open_float()<CR>",
    { noremap = true, silent = true }
)
vim.api.nvim_set_keymap("n", "<leader>r",
    "<cmd>lua vim.lsp.buf.hover()<CR>",
    { noremap = true, silent = true }
)
