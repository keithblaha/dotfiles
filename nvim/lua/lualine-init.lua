-- let lualine show the mode
vim.opt.showmode = false

require("lualine").setup({
  options = {
    theme = "catppuccin",
    ignore_focus = { "NvimTree" },
  }
})
